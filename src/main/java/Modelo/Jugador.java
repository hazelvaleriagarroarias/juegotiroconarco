/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author hazel
 */
public class Jugador implements Comparable<Jugador>{

    private String nombre;
    private int puntuacion;

    public static final String[] ETIQUETAS_JUGADOR = {"NOMBRE", "PUNTUACION"};

    public Jugador() {
    }

    public Jugador(String nombre, int puntuacion) {
        this.nombre = nombre;
        this.puntuacion = puntuacion;
    }

    public String getDatos(int indice) {
        switch (indice) {
            case 0:
                return nombre;
            case 1:
                return String.valueOf(puntuacion);
        }
        return null;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }

    @Override
    public String toString() {
        return "Jugador{" + "nombre=" + nombre + ", puntuacion=" + puntuacion + '}';
    }

    @Override
    public int compareTo(Jugador o) {
        if(o.getPuntuacion()<puntuacion){
            return -1;
        }else if(o.getPuntuacion()==puntuacion){
            return 0;
        }else{
            return 1;
        }
    }
    
}
