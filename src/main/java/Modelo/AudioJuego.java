/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import java.applet.AudioClip;

/**
 *
 * @author hazel
 */
public class AudioJuego {

    private AudioClip musicaInicio;
    private AudioClip sonidoFlecha;
    private AudioClip musicaJuego;
    private AudioClip musicaVictoria;
    private AudioClip musicaDerrota;

    public AudioJuego() {
    }

    public void musicaInicio() {
        musicaInicio = java.applet.Applet.newAudioClip(getClass().getResource("/sonidos/musicaInicio.wav"));
        musicaInicio.play();
    }

    public void musicaJuego() {
        musicaJuego = java.applet.Applet.newAudioClip(getClass().getResource("/sonidos/musicaJuego.wav"));
        musicaJuego.loop();
    }

    public void musicaVictoria() {
        musicaVictoria = java.applet.Applet.newAudioClip(getClass().getResource("/sonidos/victoria.wav"));
        musicaVictoria.play();
    }

    public void musicaDerrota() {
        musicaDerrota = java.applet.Applet.newAudioClip(getClass().getResource("/sonidos/derrota.wav"));
        musicaDerrota.loop();
    }

    public void lanzamientoFlecha() {
        sonidoFlecha = java.applet.Applet.newAudioClip(getClass().getResource("/sonidos/tiroFlecha.wav"));
        sonidoFlecha.play();
    }

    public void detenerMusicaInicio() {
        // musicaInicio = java.applet.Applet.newAudioClip(getClass().getResource(/sonidos/musicaInicio.wav"));
        musicaInicio.stop();
    }

    public void detenerMusicaJuego() {
        // musicaInicio = java.applet.Applet.newAudioClip(getClass().getResource(/sonidos/musicaInicio.wav"));
        musicaJuego.stop();
    }

}
