/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import static Controlador.ControladorJuego.abajo;
import Vista.GUIJuego;
import Vista.PanelFondoJuego;
import java.awt.Graphics;
import javax.swing.ImageIcon;

/**
 *
 * @author hazel
 */
public class HiloDiana extends Thread {

    private PanelFondoJuego panelFondo;
    public static boolean moverDiana;
    private int tiempo;
    private int x, y;
    private ImageIcon imagen;

    public HiloDiana(int x, int y, ImageIcon imagen, PanelFondoJuego panelFondo) {
        this.x = x;
        this.y = y;
        this.imagen = imagen;
        this.panelFondo = panelFondo;
        this.tiempo = 20;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public ImageIcon getImagen(ImageIcon imagen) {
        return imagen;
    }

    public void setImagen(ImageIcon imagen) {
        this.imagen = imagen;
    }

    public void dibuajr(Graphics g) {
        imagen.paintIcon(null, g, x, y);
    }

    public void run() {

        while (true) {
            try {
                sleep(this.tiempo);

                synchronized (this) {

                    if (HiloDiana.moverDiana && getY() >= 170) {
                        //movimiento de diana
                        setY(getY() + 1);
                        //panel.repaint();
                        panelFondo.repaint();
                        //System.out.println(getY());
                        if (getY() == 350) {

                            HiloDiana.moverDiana = false;
                        }
                    }

                    if ((!(HiloDiana.moverDiana)) && getY() <= 400) {
                        // movimiento de diana
                        setY(getY() - 1);
                        //panel.repaint();
                        panelFondo.repaint();
                        //System.out.println(getY());
                        if (getY() == 170) {
                            HiloDiana.moverDiana = true;
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("error en el thread" + e.getMessage());

            }
        }
    }

}
