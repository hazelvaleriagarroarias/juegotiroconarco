/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Vista.PanelFondoJuego;
import java.awt.Graphics;
import javax.swing.ImageIcon;

/**
 *
 * @author hazel
 */
public class HiloFlecha extends Thread {

    private PanelFondoJuego panelFondo;
    private Personaje personaje;
    public static boolean moverFlecha;
    public static boolean colision;
    private int tiempo;
    private int x, y;
    private ImageIcon imagen;
    private boolean colisionObstaculo;
    private boolean colisionDiana;
    private HiloDiana diana;
    private HiloObstaculo obstaculo;
    private Jugador jugador;

    public HiloFlecha(int x, int y, ImageIcon imagen, PanelFondoJuego panelFondo, Personaje personaje, HiloDiana diana, HiloObstaculo obstaculo, Jugador jugador) {
        this.x = x;
        this.y = y;
        this.imagen = imagen;
        this.panelFondo = panelFondo;
        this.tiempo = 1;
        this.personaje = personaje;
        this.diana = diana;
        this.obstaculo = obstaculo;
        this.jugador = jugador;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public ImageIcon getImagen(ImageIcon imagen) {
        return imagen;
    }

    public void setImagen(ImageIcon imagen) {
        this.imagen = imagen;
    }

    public void dibujar(Graphics g) {
        imagen.paintIcon(null, g, x, y);
    }

    public void run() {

        while (true) {
            try {
                sleep(this.tiempo);

                synchronized (this) {
                    //metodo para evaluar que se llega al borde de la pantalla y la flecha se esta moviendo 
                    if (HiloFlecha.moverFlecha && getX() <= diana.getX() - 82) {
                        setX(getX() + 1);
                        panelFondo.repaint();
                        //metodo para evaluar que se llega al borde de la pantalla 
                        if (getX() == diana.getX() - 82) {
                            /////////////////////////////////////////////////////      
                            //metodo para evaluar que se toca la diana 
                            if (getY() >= diana.getY() && getY() <= diana.getY() + 110) {

                                //metodo para evaluar que toco el centro de la diana
                                if (getY() >= diana.getY() + 41 && getY() <= diana.getY() + 71) {
                                    //se suma puntaje
                                    this.jugador.setPuntuacion(this.jugador.getPuntuacion() + 10);
                                    System.out.println("toco el centro");
                                }
                                //metodo para evaluar que toco el borde exterior de la diana
                                if ((getY() >= diana.getY() && getY() <= diana.getY() + 20) || (getY() >= diana.getY() + 90 && getY() <= diana.getY() + 110)) {
                                    //se suma puntaje
                                    this.jugador.setPuntuacion(this.jugador.getPuntuacion() + 2);
                                    System.out.println("toco el borde externo");
                                }
                                //metodo para evaluar que toco el borde interior de la diana 
                                if ((getY() >= diana.getY() + 21 && getY() <= diana.getY() + 40) || (getY() >= diana.getY() + 72 && getY() <= diana.getY() + 89)) {
                                    //se suma puntaje
                                    this.jugador.setPuntuacion(this.jugador.getPuntuacion() + 5);
                                    System.out.println("toco el borde interno");
                                }

                            }
                            HiloFlecha.colision = false;
                            //si la flecha llega al final de la pantalla y toca la diana                       

                            /////////////////////////////////////////////////
                            System.out.println("y flecha" + getY());
                            System.out.println("x flecha" + getX());
                            System.out.println("y diana" + diana.getY());
                            System.out.println("x diana" + diana.getX());
                            //se repinta la flecha fuera de la vista
                            this.setY(personaje.getY() + 3300);
                            this.setX(personaje.getX() + 3300);
                            //se finaliza la colision al ponerla en false
                            HiloFlecha.colision = true;
                            //se reinicia la imagen de la arquera
                            personaje.setImagen(new ImageIcon("./src/main/resources/imagenes/personajeDos.png"));
                            panelFondo.repaint();
                            //se suspende el hilo momentaneamente al llegar al borde de la pantalla
                            this.suspend();

                            // HiloFlecha.moverFlecha = false;
                        }
                    }
                    //si la flecha llega al final de la pantalla +50
////////////////////////////////////////Enemigo//////////////////////////////////////////////////////////

                    // si lo toca que se detenga   
                    //metodo para evaluar que se llega al borde de la pantalla 
                    if ((getX() == obstaculo.getX() - 82) && (getY() >= obstaculo.getY() && getY() <= obstaculo.getY() + 55)) {

                        ///////////////////////////////////////////////////// 
                        // la flecha llega al final de la pantalla y toca la diana 
                        /////////////////////////////////////////////////
                        System.out.println("y flechaO" + getY());
                        System.out.println("x flechaO" + getX());
                        System.out.println("y obstaculo" + obstaculo.getY());
                        System.out.println("x obstaculo" + obstaculo.getX());
                        //se repinta la flecha fuera de la vista
                        this.setY(personaje.getY() + 3300);
                        this.setX(personaje.getX() + 3300);
                        //se finaliza la colision al ponerla en false
                        HiloFlecha.colision = true;
                        //se reinicia la imagen de la arquera
                        personaje.setImagen(new ImageIcon("./src/main/resources/imagenes/personajeDos.png"));
                        panelFondo.repaint();
                        //se suspende el hilo momentaneamente al llegar al borde de la pantalla
                        this.suspend();

                        // HiloFlecha.moverFlecha = false;
                    }

/////////////////////////////////////////////////////////////////////////////////////////////////
                }
            } catch (Exception e) {
                System.out.println("error en el HILO FLECHA" + e.getMessage());

            }
        }
    }

}
