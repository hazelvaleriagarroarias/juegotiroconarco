/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author hazel
 */
public class Estadisticas {

    private ArrayList<Jugador> listaJugadores;
    private File archivo;
    private JSONObject baseJSONJugadores;

    public Estadisticas() {
        listaJugadores = new ArrayList<Jugador>();
        archivo = new File("estadisticasJugadores.json");
        leerJSON();
    }

    public void escribirJSON() {
        JSONArray arregloJugadores = new JSONArray();
        baseJSONJugadores = new JSONObject();
        for (Jugador jugador : listaJugadores) {
            JSONObject objJSONJugador = new JSONObject();
            String tempPuntuacion = jugador.getPuntuacion() + "";
            objJSONJugador.put("nombre", jugador.getNombre());
            objJSONJugador.put("puntuacion", String.valueOf(tempPuntuacion));

            arregloJugadores.add(objJSONJugador);
        }
        baseJSONJugadores.put("listaJugadores", arregloJugadores);

        try {
            FileWriter archivoEscribe = new FileWriter(archivo);
            archivoEscribe.write(baseJSONJugadores.toJSONString());
            archivoEscribe.flush();
            archivoEscribe.close();
        } catch (IOException ex) {
            System.err.println("Error al escribir en el archivo.");
        }
    }

    public void leerJSON() {
        listaJugadores = new ArrayList<>();
        JSONParser convierte = new JSONParser();
        try {
            FileReader archivoLee = new FileReader(archivo);
            Object obj = convierte.parse(archivoLee);
            baseJSONJugadores = (JSONObject) obj;

            JSONArray arregloJSON = (JSONArray) baseJSONJugadores.get("listaJugadores");
            for (Object object : arregloJSON) {
                JSONObject objJugador = (JSONObject) object;
                Jugador jugador = new Jugador();
                jugador.setNombre(objJugador.get("nombre").toString());
                jugador.setPuntuacion(Integer.parseInt(objJugador.get("puntuacion").toString()));
                listaJugadores.add(jugador);
            }

        } catch (FileNotFoundException ex) {
            System.err.println("Archivo no encontrado");
        } catch (IOException ex) {
            System.err.println("Error al leer");
        } catch (org.json.simple.parser.ParseException ex) {
            ex.printStackTrace();
        }
    }

    public String[][] getDatosTabla() {
        //listaJugadores = jugadoresOrdenada
        Collections.sort(listaJugadores);
        String[][] datos = new String[listaJugadores.size()][Jugador.ETIQUETAS_JUGADOR.length];
        for (int indice = 0; indice < listaJugadores.size(); indice++) {
            for (int atributo = 0; atributo < datos[indice].length; atributo++) {
                datos[indice][atributo] = listaJugadores.get(indice).getDatos(atributo);
            }
        }
        return datos;
    }

    public String agregar(Jugador jugador) {

        if (listaJugadores.contains(jugador)) {
            return "El jugador ya estaba registrado";
        } else {
            if (listaJugadores.add(jugador)) {
                escribirJSON();
                return "El jugador se registró correctamente";
            } else {
                return "Error al registrar jugador";
            }
        }
    }

    public String toString() {
        String salida = "Lista de jugadores\n";
        //for(int indice=0;indice<listaEstudios.size();indice++){
        for (Jugador jugador : listaJugadores) {
            //salida+=listaEstudios.get(indice)+"\n";
            salida += jugador + "\n";
        }
        return salida;
    }

   
    

}
