/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Vista.GUIJuego;
import Vista.GUIPrincipal;
import java.awt.Graphics;
import javax.swing.ImageIcon;

/**
 *
 * @author hazel
 */
public class Personaje {

    private int x, y;
    private ImageIcon imagen;

    public Personaje(int x, int y, ImageIcon imagen) {
        this.x = x;
        this.y = y;
        this.imagen = imagen;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    
    public ImageIcon getImagen(){
        return imagen;
    }
    
    public void setImagen(ImageIcon imagen){
        this.imagen= imagen;
    }
    
    public void dibuajr (Graphics g){
        imagen.paintIcon(null, g, x, y);
    }

}
