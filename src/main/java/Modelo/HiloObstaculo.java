/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Vista.PanelFondoJuego;
import java.awt.Graphics;
import javax.swing.ImageIcon;

/**
 *
 * @author hazel
 */
public class HiloObstaculo extends Thread {

    private PanelFondoJuego panelFondo;
    private int tiempo;
    private int x, y;
    private ImageIcon imagen;
    public static boolean moverObstaculo;
    //private AudioJuego audio;

    public HiloObstaculo(int x, int y, ImageIcon imagen) {
        this.x = x;
        this.y = y;
        this.imagen = imagen;
        this.panelFondo = panelFondo;
        this.tiempo = 45;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public ImageIcon getImagen(ImageIcon imagen) {
        return imagen;
    }

    public void setImagen(ImageIcon imagen) {
        this.imagen = imagen;
    }

    public void dibujar(Graphics g) {
        imagen.paintIcon(null, g, x, y);
    }

    public void run() {

        while (true) {
            try {
                sleep(this.tiempo);

                synchronized (this) {
                    
                    setY(getY() - 1);
                    //panel.repaint();

                    // System.out.println(getY());
                    if (getY() <= 170 && HiloObstaculo.moverObstaculo) {
                        int randomX = (int) (Math.random() * (490 - 100 + 1) + 100);
                        int randomY = (int) (Math.random() * (400 - 169 + 1) + 169);
                        setX(randomX);
                        setY(randomY);
                    }
                    //  }

                }
            } catch (Exception e) {
                System.out.println("error en el Hilo Obstaculo" + e.getMessage());

            }
        }
    }

}
