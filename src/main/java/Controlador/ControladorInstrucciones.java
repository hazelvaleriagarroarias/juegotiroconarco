/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Vista.GUIInstrucciones;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.Icon;

/**
 *
 * @author hazel
 */
public class ControladorInstrucciones implements MouseListener, MouseMotionListener {
    
    private GUIInstrucciones guiInstrucciones;
    
    public ControladorInstrucciones(GUIInstrucciones gui) {

        this.guiInstrucciones =gui;
    }

    @Override
    public void mouseClicked(MouseEvent evento) {
        if (evento.getSource() == this.guiInstrucciones.getSalirPrincipal()) {
            guiInstrucciones.dispose();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent evento) {
        Icon imagenSalir2  =new javax.swing.ImageIcon(getClass().getResource("/imagenes/salirDos.png")) ;
        
        if(evento.getSource()==this.guiInstrucciones.getSalirPrincipal()){
          
                  this.guiInstrucciones.setImagenSalir(imagenSalir2);
              
          }
    }

    @Override
    public void mouseExited(MouseEvent evento) {
        Icon imagenSalir1  =new javax.swing.ImageIcon(getClass().getResource("/imagenes/salirUno.png")) ;
        
        if(evento.getSource()==this.guiInstrucciones.getSalirPrincipal()){
              
                 this.guiInstrucciones.setImagenSalir(imagenSalir1);
          }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }
    
    
}
