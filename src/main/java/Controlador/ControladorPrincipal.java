/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.AudioJuego;
import Modelo.Estadisticas;
import Modelo.Jugador;
import Vista.GUICreditos;
import Vista.GUIEstadisticas;
import Vista.GUIInstrucciones;
import Vista.GUIJuego;
import Vista.GUIPrincipal;
import Vista.PanelInstrucciones;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.Icon;
import javax.swing.JOptionPane;

/**
 *
 * @author hazel
 */
public class ControladorPrincipal implements MouseListener, MouseMotionListener {

    private GUIPrincipal guiPrincipal;
    private GUIInstrucciones guiInstrucciones;
    private GUIJuego guiJuego;
    private GUIEstadisticas guiEstadisticas;
    public static Jugador jugador = new Jugador("Player 1", 0);
    private AudioJuego audio;
    private Estadisticas estadisticas;
    private GUICreditos guiCreditos;

    public ControladorPrincipal(GUIPrincipal gui) {

        this.guiPrincipal = gui;
        this.guiInstrucciones = new GUIInstrucciones();
        this.guiJuego = new GUIJuego();
        this.estadisticas = new Estadisticas();
        this.guiCreditos = new GUICreditos();
        this.estadisticas.leerJSON();
        this.guiEstadisticas = new GUIEstadisticas(estadisticas.getDatosTabla());
        this.audio = new AudioJuego();
    }

    @Override
    public void mouseClicked(MouseEvent evento) {
        if (evento.getSource() == this.guiPrincipal.getJugar()) {
            //audio.musicaInicio();

            audio.detenerMusicaInicio();
            String nombreJugador = JOptionPane.showInputDialog("Digite su nombre para iniciar la sesion de juego");
            ControladorPrincipal.jugador.setNombre(nombreJugador);

            audio.detenerMusicaInicio();
            this.guiJuego.setVisible(true);
            audio.musicaJuego();

        }
        if (evento.getSource() == this.guiPrincipal.getInstrucciones()) {
            audio.musicaInicio();
            this.guiInstrucciones.setVisible(true);
        }
        if (evento.getSource() == this.guiPrincipal.getSalir()) {
            System.exit(0);
        }

        if (evento.getSource() == this.guiPrincipal.getEstadisticas()) {
            //audio.detenerMusicaJuego();
            //audio.musicaInicio();
            this.estadisticas.leerJSON();
            this.guiEstadisticas.setDatosTabla(estadisticas.getDatosTabla());
            this.guiEstadisticas.setVisible(true);
        }
        if (evento.getSource() == this.guiPrincipal.getCreditos()) {
            this.guiCreditos.setVisible(true);
        }
    }

    @Override
    public void mousePressed(MouseEvent evento) {
    }

    @Override
    public void mouseReleased(MouseEvent evento) {
    }

    @Override
    public void mouseEntered(MouseEvent evento) {
        Icon imagenJugar2 = new javax.swing.ImageIcon(getClass().getResource("/imagenes/jugarDos.png"));
        Icon imagenInstrucciones2 = new javax.swing.ImageIcon(getClass().getResource("/imagenes/instruccionesDos.png"));
        Icon imagenSalir2 = new javax.swing.ImageIcon(getClass().getResource("/imagenes/salirDos.png"));
        Icon imagenEstadisticas2 = new javax.swing.ImageIcon(getClass().getResource("/imagenes/estadisticasDos.png"));
        Icon imagenCreditos2 = new javax.swing.ImageIcon(getClass().getResource("/imagenes/creditosDos.png"));

        if (evento.getSource() == this.guiPrincipal.getJugar()) {
            this.guiPrincipal.setImagenJugar(imagenJugar2);
        }

        if (evento.getSource() == this.guiPrincipal.getInstrucciones()) {
            this.guiPrincipal.setImagenInstrucciones(imagenInstrucciones2);
        }

        if (evento.getSource() == this.guiPrincipal.getSalir()) {
            this.guiPrincipal.setImagenSalir(imagenSalir2);
        }

        if (evento.getSource() == this.guiPrincipal.getEstadisticas()) {
            this.guiPrincipal.setImagenEstadisticas(imagenEstadisticas2);
        }
        
        if (evento.getSource() == this.guiPrincipal.getCreditos()) {
            this.guiPrincipal.setImagenCreditos(imagenCreditos2);
        }

    }

    @Override
    public void mouseExited(MouseEvent evento) {
        Icon imagenJugar1 = new javax.swing.ImageIcon(getClass().getResource("/imagenes/jugarUno.png"));
        Icon imagenInstrucciones1 = new javax.swing.ImageIcon(getClass().getResource("/imagenes/instruccionesUno.png"));
        Icon imagenSalir1 = new javax.swing.ImageIcon(getClass().getResource("/imagenes/salirUno.png"));
        Icon imagenEstadisticas1 = new javax.swing.ImageIcon(getClass().getResource("/imagenes/estadisticasUno.png"));
        Icon imagenCreditos1 = new javax.swing.ImageIcon(getClass().getResource("/imagenes/creditosUno.png"));

        if (evento.getSource() == this.guiPrincipal.getJugar()) {
            this.guiPrincipal.setImagenJugar(imagenJugar1);
        }

        if (evento.getSource() == this.guiPrincipal.getInstrucciones()) {
            this.guiPrincipal.setImagenInstrucciones(imagenInstrucciones1);
        }

        if (evento.getSource() == this.guiPrincipal.getSalir()) {
            this.guiPrincipal.setImagenSalir(imagenSalir1);
        }

        if (evento.getSource() == this.guiPrincipal.getEstadisticas()) {
            this.guiPrincipal.setImagenEstadisticas(imagenEstadisticas1);
        }
        
        if (evento.getSource() == this.guiPrincipal.getCreditos()) {
            this.guiPrincipal.setImagenCreditos(imagenCreditos1);
        }

    }

    @Override
    public void mouseDragged(MouseEvent evento) {
    }

    @Override
    public void mouseMoved(MouseEvent evento) {
    }

}
