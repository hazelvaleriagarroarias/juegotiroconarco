/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.AudioJuego;
import Modelo.Estadisticas;
import Modelo.HiloDiana;
import Modelo.HiloFlecha;
import Modelo.HiloObstaculo;
import Modelo.Jugador;
import Modelo.Personaje;
import Vista.GUIDerrota;
import Vista.GUIJuego;
import Vista.GUIPrincipal;
import Vista.GUIVictoria;
import Vista.PanelFondoJuego;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author hazel
 */
public class ControladorJuego implements KeyListener, MouseListener, MouseMotionListener {

    private Personaje personaje;
    private ImageIcon imagen1;
    private ImageIcon imagen2;
    private PanelFondoJuego panel;
    public static boolean arriba, abajo;
    private HiloDiana hiloDiana;
    private GUIJuego guiJuego;
    private HiloFlecha hiloFlecha;
    private HiloObstaculo hiloObstaculo;
    private int nivel;
    private int cantidadFlechas;
    private Jugador jugador;
    private GUIVictoria guiVictoria;
    private GUIDerrota guiDerrota;
    private AudioJuego audio;
    private Estadisticas estadisticas;

    public ControladorJuego(PanelFondoJuego panel) {
        this.personaje = new Personaje(50, 264, new ImageIcon("./src/main/resources/imagenes/personajeDos.png"));
        this.panel = panel;
        this.hiloDiana = new HiloDiana(900, 243, new ImageIcon("./src/main/resources/imagenes/diana.png"), panel);
        this.hiloObstaculo = new HiloObstaculo(550, 340, new ImageIcon("./src/main/resources/imagenes/obstaculo.png"));
        HiloDiana.moverDiana = true;
        HiloFlecha.moverFlecha = false;
        this.jugador = ControladorPrincipal.jugador;
        HiloFlecha.colision = false;
        this.hiloFlecha = new HiloFlecha(personaje.getX(), personaje.getY() + 33, new ImageIcon("./src/main/resources/imagenes/flecha.png"), panel, personaje, hiloDiana, hiloObstaculo, jugador);
        this.hiloDiana.start();
        this.hiloObstaculo.start();
        this.hiloFlecha.start();
        hiloObstaculo.moverObstaculo = true;
        this.nivel = 1;
        this.cantidadFlechas = 7;
        this.guiVictoria = new GUIVictoria();
        this.guiDerrota = new GUIDerrota();
        this.audio = new AudioJuego();
        this.estadisticas = new Estadisticas();
    }

    public void dibujar(Graphics g) {

        if (HiloFlecha.moverFlecha || HiloFlecha.colision) {
            hiloFlecha.dibujar(g);
        }
        personaje.dibuajr(g);
        hiloDiana.dibuajr(g);
        hiloObstaculo.dibujar(g);
    }

    @Override
    public void keyTyped(KeyEvent evento) {
    }

    @Override
    public void keyPressed(KeyEvent evento) {
        int code = evento.getKeyCode();

        if (code == KeyEvent.VK_W) {
            if (personaje.getY() > 210) {
                arriba = true;
                personaje.setY(personaje.getY() - 5);
                //panel.repaint();      
                panel.repaint();
                // System.out.println(personaje.getY());
            }
        }//Fin if

        if (code == KeyEvent.VK_S) {
            if (personaje.getY() < 400) {
                abajo = true;
                personaje.setY(personaje.getY() + 5);
                panel.repaint();
                // System.out.println(personaje.getY());
            }
        }//Fin if
        
        if (code == KeyEvent.VK_SPACE) {
            this.cantidadFlechas -= 1;
            if (this.jugador.getPuntuacion() >= 15 && nivel == 1) {
                this.nivel += 1;
                this.cantidadFlechas = 9;
                hiloDiana.setTiempo(15);
                hiloObstaculo.setTiempo(40);
                //mensaje de nivel superado 
                JOptionPane.showMessageDialog(null, "Excelente " + ControladorPrincipal.jugador.getNombre() + ", has logrado " + ControladorPrincipal.jugador.getPuntuacion() + "puntos, nivel 1 superado");
            }
            if (nivel == 2 && this.jugador.getPuntuacion() >= 30) {
                this.nivel += 1;
                this.cantidadFlechas = 11;
                hiloDiana.setTiempo(10);
                hiloObstaculo.setTiempo(35);
                //mensaje de nivel superado 
                JOptionPane.showMessageDialog(null, "Excelente " + ControladorPrincipal.jugador.getNombre() + ", has logrado " + ControladorPrincipal.jugador.getPuntuacion() + "puntos, nivel 2 superado");
            }
            if (nivel == 3 && this.jugador.getPuntuacion() >= 45) {
                //aqui se escribe el puntaje en el json
                estadisticas.agregar(ControladorPrincipal.jugador);
                //mensaje de victoria 
                JOptionPane.showMessageDialog(null, "Felicidades " + ControladorPrincipal.jugador.getNombre() + ", has ganado con " + ControladorPrincipal.jugador.getPuntuacion() + " puntos en total");
                this.guiVictoria.setVisible(true);
                this.nivel = 1;
                this.cantidadFlechas = 7;
                hiloDiana.setTiempo(20);
                hiloObstaculo.setTiempo(45);
                ControladorPrincipal.jugador.setPuntuacion(0);
            }

            if (cantidadFlechas == -1) {
                //mensaje de derrota 
                //aqui se escribe el puntaje en el json
                estadisticas.agregar(ControladorPrincipal.jugador);
                //mensaje de mensaje de derrota 
                JOptionPane.showMessageDialog(null, "Oh no, que mal " + ControladorPrincipal.jugador.getNombre() + ", has perdido");

                this.guiDerrota.setVisible(true);
                //se reinicia todo como en el nivel 1
                this.nivel = 1;
                this.cantidadFlechas = 7;
                hiloDiana.setTiempo(20);
                hiloObstaculo.setTiempo(45);
                ControladorPrincipal.jugador.setPuntuacion(0);
            }

            hiloFlecha.resume();
            HiloFlecha.moverFlecha = true;
            audio.lanzamientoFlecha();
            // System.out.println("diana Y es "+hiloDiana.getY());
            // System.out.println("flecha Y es "+hiloFlecha.getY());           
            this.personaje.setImagen(new ImageIcon("./src/main/resources/imagenes/personajeUno.png"));
            panel.repaint();
            //}

            if (HiloFlecha.colision) {
                hiloFlecha.setX(personaje.getX());
                hiloFlecha.setY(personaje.getY() + 33);
                HiloFlecha.moverFlecha = true;
                HiloFlecha.colision = false;
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent evento) {
    }

    @Override
    public void mouseClicked(MouseEvent evento) {
    }

    @Override
    public void mousePressed(MouseEvent evento) {
    }

    @Override
    public void mouseReleased(MouseEvent evento) {
    }

    @Override
    public void mouseEntered(MouseEvent evento) {
    }

    @Override
    public void mouseExited(MouseEvent evento) {
    }

    @Override
    public void mouseDragged(MouseEvent evento) {
    }

    @Override
    public void mouseMoved(MouseEvent evento) {
    }

}
